function [res_0, res, field_vars, coupling_vars_out, history_vars_new, non_conv] = newton_raphson(...
    assembly_fh,...
    sys,...
    field_vars,...
    field_vars_old,...
    coupling_vars_in,...
    history_vars_old,...
    active_dof,...
    i_row,...
    j_col,...
    args...
)
%NEWTON_RAPHSON Newton-Raphson iterative solver of the phase field equation
%  ...

% Copyright (C) 2021-2023 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

arguments
    assembly_fh (1, 1) function_handle
    sys (1, 1) handle
    field_vars (:, :) double
    field_vars_old (:, :) double
    coupling_vars_in (:, :) double
    history_vars_old (:, :, :) double
    active_dof (:, :) double
    i_row (:, 1) double
    j_col (:, 1) double
    args.max_iter (1, 1) double {mustBeInteger, mustBePositive} = 500
    args.min_iter (1, 1) double {mustBeInteger, mustBePositive} = 1
    args.res_tol (1, 1) double {mustBePositive} = 1e-6
    args.D_RHS_tract (:, 1) double = 0.0
    args.line_search (1,1) logical = false
    args.line_search_maxiter (1,1) = 20
end

non_conv = false;

%iterative solution process
for n_iter=1:args.max_iter

    %call assembly routine
    [K_vect, res_field_vars, coupling_vars_out, history_vars_new] = assembly_fh(...
        sys.MESH, sys.DOFS, sys.GEOM.t,...
        sys.QUADRATURE, sys.MAT_CHAR, sys.CC,...
        field_vars, field_vars_old, coupling_vars_in,...
        sys.stress_state.as_number,...
        history_vars_old...
    );

    %assemble sparse matrix
    KK = sparse(i_row, j_col, K_vect);

    %subtract external RHS vector if existent
    if any(args.D_RHS_tract ~= 0.0)
        res_field_vars = res_field_vars - args.D_RHS_tract;
    end
    
    %solve the system equation
    Dfield_vars = cholmod2(KK(active_dof, active_dof), -res_field_vars(active_dof, 1));

    if (n_iter > args.line_search_maxiter) && args.line_search
        field_vars = phase_field.fem.solver.line_search(...
            sys, ...
            field_vars, ...
            field_vars_old, ...
            coupling_vars_in, ...
            history_vars_old, ...
            active_dof, ...
            Dfield_vars, ...
            assembly_fh, ...
            args.D_RHS_tract, ...
            args.res_tol);  
    else
        field_vars(active_dof, 1) = field_vars(active_dof, 1) + Dfield_vars;
    end

    %compute residual norm
    res = norm(res_field_vars(active_dof,1));
    if (n_iter == 1)
        res_0 = res;
    end
    fprintf('         Newton-Raphson iteration:  %d  res_norm:   %g\n', n_iter, res);

    % convergence test
    if n_iter >= args.min_iter && res < args.res_tol
        break;
    end

    if n_iter == args.max_iter
        warning('         Newton-Raphson did not converge!')
        non_conv = true;
        break;
    end

end

end
