% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% =========================================================================

model_extension = 'FATIGUE';

split_type = 'ISO';

diss_fct = 'AT2';

irrev   = 'HISTORY';

example_name = 'SENT';


% =========================================================================
%   MATERIAL & MODEL PROPERTIES
% =========================================================================
[MESH, GEOM] = specimen.external.abaqus_import(...
    'Dependencies/SENT_mesh/SENT_mesh.inp', ...
    't',1 ...
    );

MAT_CHAR(1) = phase_field.init.material_characteristic(...
    GEOM.L, diss_fct, 'E',210000,'ni',0.3,'Gc',2.7,'ell',0.004, 'res_stiff', 1e-6,'alpha_T',56.25,'p',2.0 ...
    );

MESH.elem_material_id = ones(MESH.num_elem, 1);

bottom = find(MESH.node(:,2)==-0.5);
bottom_left = bottom(1);
top = find(MESH.node(:,2)==0.5);

NODE_BOUNDARIES = phase_field.fem.bc.set_boundaries(...
    'fix_X', bottom_left, ...
    'fix_Y', bottom, ...
    'disp_Y', top ...
    );

DOFS = phase_field.init.dofs(MESH, NODE_BOUNDARIES);

QUADRATURE = phase_field.init.quadrature(MESH.dim);

stress_state = phase_field.StressState.PlaneStrain;


% =========================================================================
%   SOLVER PARAMETERS
% =========================================================================

SOL_PAR = phase_field.fem.solver.params('tol_displ', 1e-6, 'tol_p_field', 1e-6, 'max_iter_displ', 250, 'max_iter_pf', 250);

SOL_STAG_PAR = phase_field.fem.solver.stag.params('tol',1e-4,'max_iter', 1000);

SOL_STEP_PAR = phase_field.fem.solver.step.params(...
    'n_step', 150, ...
    'uy_final', 6e-3...
    );

SOL_CYCL_PAR = phase_field.fem.solver.cycl.params(...
    'max_cycle', 1, ...
    'da_paraview',0 ...
    );

SOL_JUMP_PAR = phase_field.fem.solver.cycl_jump.params(...
    'cycl_jump',false...
    );


CRACK_PAR = phase_field.fem.crack.params(...
    MAT_CHAR, diss_fct...
    );
