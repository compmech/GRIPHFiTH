% Copyright (C) 2021-2023 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% =========================================================================
%   COMPUTE CYCLES
% =========================================================================

%cycle loop
while SOL_CYCL_VAR.n_cycle <= (SOL_CYCL_PAR.max_cycle-1)

    % =========================================================================
    %   STEP-BY-STEP SOLUTION
    % =========================================================================
    cycle_tic = tic;
    
    %count up explicitly computed cycle
    SOL_CYCL_VAR.n_cycle = SOL_CYCL_VAR.n_cycle + 1;

    % ========STEP ITERATION===================================================
    for i_step = 1:SOL_STEP_PAR.n_step
        disp('=========================================================================')
        disp([' CYCLE Nr.:', num2str(SOL_CYCL_VAR.n_cycle), ' STEP Nr.: ', num2str(i_step)]);
        disp('=========================================================================')

        [displ, res_displ, D_RHS_tract] = phase_field.fem.solver.step.pre_iter_update(i_step, sys, displ, SOL_STEP_PAR);
        res_displ_0 = norm(res_displ(sys.DOFS.active_dof,1));

        % ========STAGGERED ITERATION==============================================
        for i_stag = 1:SOL_STAG_PAR.max_iter
            disp([' STAG ITER Nr.: ', num2str(i_stag), '---------------------------']);

            % ======NEWTON-RAPHSON ITERATION FOR THE EQUILIBRIUM EQUATION==========
            [res_displ_0, res_displ, displ, strain_en_undgr, ~, non_conv] = phase_field.fem.solver.newton_raphson(...
                assembly_equilibrium_fh,...
                sys,...
                displ, 0.0, p_field,...
                0.0,...
                sys.DOFS.active_dof,...
                sys.STIFFNESS_MATRIX.i_row,...
                sys.STIFFNESS_MATRIX.j_col,...
                'D_RHS_tract', D_RHS_tract,...
                'max_iter', SOL_PAR.max_iter_displ,...
                'res_tol', SOL_PAR.tol_displ,...
                'line_search', SOL_STEP_PAR.line_search...
            );
            if isnan(res_displ) || non_conv
                phase_field.fem.crack.fatigue_life(SOL_CYCL_VAR.n_cycle, sys, CRACK_PAR, SOL_STEP_PAR, example_name);
                error('displacement residual is NaN or Newton-Raphson did not converge');
            end

            % =======NEWTON-RAPHSON ITERATION FOR THE PHASE FIELD EQUATION=========
            [i_res_pf_0, res_pf, p_field, ~, history_vars_new, non_conv] = phase_field.fem.solver.newton_raphson(...
                assembly_pf_fh,...
                sys,...
                p_field, p_field_old, strain_en_undgr,...
                history_vars_old,...
                sys.DOFS.active_dof_pf,...
                sys.STIFFNESS_MATRIX.i_row_pf,...
                sys.STIFFNESS_MATRIX.j_col_pf,...
                'max_iter', SOL_PAR.max_iter_pf,...
                'res_tol', SOL_PAR.tol_p_field,...
                'line_search', SOL_STEP_PAR.line_search...
            );
            if isnan(res_pf) || non_conv
                phase_field.fem.crack.fatigue_life(SOL_CYCL_VAR.n_cycle, sys, CRACK_PAR, SOL_STEP_PAR, example_name);
                error('phase-field residual is NaN or Newton-Raphson did not converge');
            end
            if i_stag == 1
                res_pf_0 = i_res_pf_0;
            end

            % ==================CHECK FOR STAGGERED CONVERGENCE====================
            [SOL_STAG_VAR, strain_en_undgr, stag_converged, sys.STIFFNESS_MATRIX.KK] = phase_field.fem.solver.stag.post_iter_update(...
                assembly_equilibrium_fh, i_stag, SOL_STAG_VAR,...
                sys,...
                D_RHS_tract,...
                p_field, displ,...
                res_pf_0, res_pf, res_displ_0,...
                SOL_STAG_PAR...
            );
            if stag_converged
                % update the history variables
                p_field_old = p_field;
                [~, ~, ~, history_vars_old] = assembly_pf_fh(...
                    sys.MESH, sys.DOFS, sys.GEOM.t,...
                    sys.QUADRATURE, sys.MAT_CHAR, sys.CC,...
                    p_field, p_field_old, strain_en_undgr,...
                    sys.stress_state.as_number,...
                    history_vars_old...
                );
                break;
            end

            % ==================CHECK FOR NON-CONVERGENCE OR NAN RESIDUAL====================
            if i_stag == SOL_STAG_PAR.max_iter
                phase_field.fem.crack.fatigue_life(SOL_CYCL_VAR.n_cycle, sys, CRACK_PAR, SOL_STEP_PAR, example_name);
                error('staggered loop did not converge!');
            end
        end

        % ============================STEP POST-PROCESSING=============================
        
        %convergence information
        SOL_STEP_VAR = phase_field.fem.solver.step.post_iter_update(...
            i_step, SOL_STEP_VAR,...
            sys.STIFFNESS_MATRIX, displ, sys.NODE_BOUNDARIES, sys.MESH.num_node,...
            SOL_STEP_PAR, example_name...
        );
        i_SOL_STAG_VAR = SOL_STAG_VAR(1:i_stag, :);

        indexCols = [i_step * ones(1,i_stag) ; 1:i_stag]';
        indexColumnHeader = {'Load Step', 'iter'};
        cols = [res_displ_0 * ones(i_stag,1), res_pf_0 * ones(i_stag,1),...
            i_SOL_STAG_VAR.el_en, i_SOL_STAG_VAR.fract_en, i_SOL_STAG_VAR.tot_en,...
            i_SOL_STAG_VAR.normal_res_displ, i_SOL_STAG_VAR.normal_res_pf,...
            i_SOL_STAG_VAR.normal_var_en_tot, i_SOL_STAG_VAR.normal_res,...
            i_SOL_STAG_VAR.normal_var_res...
        ];
        columnHeader = {'RES_displ(0)', 'RES_PF(0)', 'Elast_en', 'Fract_en', 'Total_en', 'RES_displ', 'RES_PF', 'Norm_EN_tot', 'Norm_RES_tot', 'Norm_var_RES'};
        logPath = [example_name filesep 'CONV_' example_name sprintf('%05d', i_step) '.log'];

        phase_field.mex.output.dat_output(indexCols, indexColumnHeader, cols, columnHeader, logPath);

        % reset SOL_STAG_VAR for the next staggered iteration
        SOL_STAG_VAR = phase_field.fem.solver.stag.vars(SOL_STAG_PAR);
        
    end
    
    % =========================================================================
    %   POST-PROCESSNG (CYCLE-WISE)
    % =========================================================================

    % ============================CRACK PROCESSING=============================
    [CRACK_VAR] = phase_field.fem.crack.crack_regularized(SOL_CYCL_VAR.n_cycle, sys, CRACK_PAR, CRACK_VAR, p_field, example_name);
    
    %crack growth rate evaluation for specific specimens
    if ismember(CRACK_PAR.specimen, {'ct', 'tpb', 'sent', 'dent', 'cct'})        
        %crack tip monitoring
        [CRACK_VAR] = phase_field.fem.crack.crack_discretized(SOL_CYCL_VAR.n_cycle, sys, CRACK_PAR, CRACK_VAR, p_field, example_name);
        [CRACK_VAR] = phase_field.fem.crack.crack_interpolated(SOL_CYCL_VAR.n_cycle, sys, CRACK_PAR, CRACK_VAR, p_field, example_name);

        %crack growth rate and stress intensity factor amplitude computation to prepare Paris plot
        [CRACK_VAR] = phase_field.fem.crack.fatigue_crack_growth(sys, CRACK_PAR, CRACK_VAR, SOL_STEP_PAR, example_name);
    elseif strcmp(CRACK_PAR.specimen, 'bar') && any(p_field>CRACK_PAR.crack_thres)
        %fatigue life output for Woehler curve
        phase_field.fem.crack.fatigue_life(SOL_CYCL_VAR.n_cycle, sys, CRACK_PAR, SOL_STEP_PAR, example_name);
        
        %output cputime for the cycle
        phase_field.mex.output.dat_output([SOL_CYCL_VAR.n_cycle 1], {'cycle' 'type'}, [cputime toc(cycle_tic)], {'totaltime' 'cycletime'}, [example_name filesep 'cputime.dat']);
        error('bar specimen has cracked, stopping computations')
    end
    
    % ============================SYSTEM MONITORING=============================
    %norm-based monitoring of the system
    [SOL_CYCL_VAR] = phase_field.fem.solver.cycl.monitor(SOL_CYCL_VAR,displ,p_field,history_vars_old(:,:,2),example_name);
    
    %display elapsed time for the cycle
    disp(['cycle computation time: ' num2str(toc(cycle_tic)) 's'])
    
    %output cputime for the cycle
    phase_field.mex.output.dat_output([SOL_CYCL_VAR.n_cycle 1], {'cycle' 'type'}, [cputime toc(cycle_tic)], {'totaltime' 'cycletime'}, [example_name filesep 'cputime.dat']);

    % ============================PARAVIEW OUTPUT=============================
    %paraview output either step-by-step (da_paraview=0 as default) or once a certain crack growth has been reached
    if (CRACK_VAR.crack_regularized(end,3)-SOL_CYCL_VAR.da_paraview_last)>=SOL_CYCL_PAR.da_paraview
        
        %prepare the L2-projection for the postprocessing
        [history_vars_L2, stress_L2] = phase_field.mex.fem.processing.L2_PROJECTION(sys.MESH, sys.DOFS, sys.GEOM.t, sys.num_materials, sys.QUADRATURE, sys.CC, p_field, 4, history_vars_old);
        
        %write paraview output file
        stress_L2_matrix = sparse(sys.STIFFNESS_MATRIX.i_row_eps, sys.STIFFNESS_MATRIX.j_col_eps, stress_L2);
        phase_field.output.paraview_step(...
            [example_name filesep 'fields_' sprintf('%06d_%03d', SOL_CYCL_VAR.n_cycle, i_step) '.vtk'],...
            sys.STIFFNESS_MATRIX, sys.MESH,...
            {'d', 'u'},...
            {p_field, reshape(displ, [sys.MESH.num_node, 2])},...
            {'epsilon', 'sigma', 'H', 'alpha', 'Dalpha', 'f(alpha)'},...
            {reshape(sys.STIFFNESS_MATRIX.eps * displ, [sys.MESH.num_node, 3]), reshape(stress_L2_matrix * displ, [sys.MESH.num_node, 3]), history_vars_L2(:,1), history_vars_L2(:,2), history_vars_L2(:,3), history_vars_L2(:,4)}...
            );
        
        %save last crack increment of output
        SOL_CYCL_VAR.da_paraview_last = CRACK_VAR.crack_regularized(end,3);
    end

    % =========================================================================
    %   CYCLE-JUMP
    % =========================================================================
    if SOL_JUMP_PAR.cycl_jump
        accelerate_fatigue_fracture;
    end

end