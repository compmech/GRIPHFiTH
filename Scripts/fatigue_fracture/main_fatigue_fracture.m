% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% =========================================================================

%set start timer
start_tic = tic;

%clear variables and output folder, formatting
clearvars -except start_tic
close all
clc

format shortE;

% =========================================================================
%add griphfith resources
dep_path = genpath(fullfile('..', '..', 'Dependencies/'));
addpath(dep_path)
pkg_path = fullfile('..', '..', 'Sources/');
addpath(pkg_path)

% =========================================================================
%display griphfith information
currenttime = datetime("now");
disp(' ');
disp('        GRIPHFITH');
disp(currenttime);
disp(' ');

% =========================================================================
%call input file
INPUT_SENT % _CT, _TPB, _PP

% =========================================================================
%initialize Griphfith
init_fatigue_fracture

%solve problem
solve_fatigue_fracture

%display totally elapsed time
disp(['total computation time: ' num2str(toc(start_tic)) 's'])
