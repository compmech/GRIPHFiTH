% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% =========================================================================

% this is the script used for the calibration of the smeared crack length (Fig. A4)
% Heinzmann et al. (2024): An adaptive acceleration scheme for phase-field fatigue computations
% https://doi.org/10.48550/arXiv.2404.07003

% parameters for the study
diss_fct = 'AT1';
split_type = 'ISO';
irrev   = 'PENALTY';
ell = 0.01;
ell_h = 5;

example_name = 'aell';

% =========================================================================
%   MESH AND PROBLEM SETUP
% =========================================================================
%create mesh
L = 1;
B = 1.5;

MAT_CHAR(1) = phase_field.init.material_characteristic(max([L, B]), diss_fct, 'ell', ell,'tol_AT1_recovery',1e-3,'tol_irrev',1e-6);
MAT_CHAR(2) = MAT_CHAR(1);


%FOR LOCAL REFINEMENT
%compute number of elements
B_ref = ell*20;
B_unref = B - B_ref;
if B_unref<0.1*B
    error('refined area is too large')
end

h_aim = ell/ell_h;
Nref_y = round(B_ref/h_aim);
if mod(Nref_y,2)==0
    Nref_y = Nref_y + 1; %ensure that there is a row of elements for the crack in the middle
end
h = 2*B_ref/(Nref_y);

%compute the rest of the elements such that in the middle, the elements are roughly square
Nx = round(L/h);
Ny = round(B_unref/(7.5*h)); %have element stretched not more by 5

[MESH, GEOM, NODESETS, ELEMSETS] = specimen.internal.plate(...
    'L', L, 'B', B, 'Nx', Nx, 'Ny', Ny, ...
    'Lref_y', B_ref, 'Nref_y', Nref_y, 'notch_type', 'phase_field');

%lower half of the left edge
NODESETS('left_lower') = find(MESH.node(NODESETS('left'),2)<B/2);
NODESETS('left_upper') = find(MESH.node(NODESETS('left'),2)>B/2);

%=======================================================================================================
%   BCs
% =========================================================================
%prevent phase-field evolution on edge of BC on u by means of BC on phase-field (except for the very middle nodes)
y = MESH.node(NODESETS('left'),2);
dist = abs(GEOM.B/2 - y);
min_dist = sortrows([NODESETS('left'), dist], 2, "ascend");
middle_nodes = min_dist(1:4,1);
NODESETS('pf_bcs') = setdiff(NODESETS('left'), middle_nodes);

NODE_BOUNDARIES = phase_field.fem.bc.set_boundaries(...
    'fix_X', NODESETS('left'), ...
    'fix_Y', NODESETS('left_lower'), ...
    'disp_Y', NODESETS('left_upper'), ...
    'hom_pf', NODESETS('pf_bcs'));

h_x = max(diff(MESH.node(:,1)));
h_y = diff(MESH.node(end/2:end/2+1,2));
J_elem = h_x*h_y;

%find crack elements/ nodes
crack_elems = [];
crack_nodes = [];
for el = 1:MESH.num_elem
    
    %get nodes and coordinates of elements
    el_nodes = MESH.elem(el,1:MESH.nel);
    el_coord = MESH.node(el_nodes,1:MESH.dim);
    
    if sum(abs(el_coord(:,2)-B/2)<=(h_y))==4
        crack_elems = [crack_elems; el];
    end
end

DOFS = phase_field.init.dofs(MESH, NODE_BOUNDARIES);

MESH.elem_material_id(crack_elems) = 2;


% =========================================================================
% QUADRATURE
% =========================================================================
QUADRATURE = phase_field.init.quadrature(MESH.dim);
stress_state = phase_field.StressState.PlaneStrain;


% =========================================================================
%   SOLVER PARAMETERS
% =========================================================================
SOL_PAR = phase_field.fem.solver.params();

SOL_STAG_PAR = phase_field.fem.solver.stag.params('max_iter', 2 * SOL_PAR.max_iter_pf);

SOL_STEP_PAR = phase_field.fem.solver.step.params('n_step', 15000, 'uy_final', 0.03, 'output_freq', 100);

CRACK_PAR = phase_field.fem.crack.params(...
    MAT_CHAR, diss_fct, ...
    'specimen', 'pulltest', ...
    'crack_dir', 1, ...
    'a_offset', 0.2, ...
    'crack_thres', 0.95, ...
    'J_elem', J_elem, ...
    'export_pf_interp', true, ...
    'crack_elems', crack_elems, ...
    'crack_nodes', crack_nodes, ...
    'smeared_computation', 'approximation', ...
    'ellh', 0);
