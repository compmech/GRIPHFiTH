% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% =========================================================================
% SPLIT..........................
% ISO     -> Isotropic model
% AMOR    -> Amor vol./dev. split
% MIEHE   -> Spectral split based on the sign of the trace
% FREDDI  -> Freddi bruco split

split_type = 'ISO';
%................................

% DISSIPATION FUNCTION...........
% AT1     -> Linear dissipation w(d) = d
% AT2     -> Quadratic dissipation w(d) = d^2

diss_fct = 'AT1';
%................................

% IRREVERSIBILITY CONDITION......
% HISTORY  -> With history variable [Miehe2010]
% PENALTY  -> With penalty parameter [Gerasimov_DeLorenzis2018]

irrev   = 'PENALTY';
%................................
% =========================================================================

example_name = strcat(split_type,'_',diss_fct,'_',irrev);

% =========================================================================
%   MESH AND PROBLEM SETUP
% =========================================================================
% MESH AND GEOMETRY.......................
[MESH, GEOM, NODESETS, ELEMSETS] = specimen.internal.plate(...
    'notch_type', 'phase_field',...
    'Nx', 22, 'Ny', 22,...
    'Lnotch_y', 0.2 ...
);
%................................

% =========================================================================
%   MATERIAL & MODEL PROPERTIES
% =========================================================================
   MAT_CHAR = phase_field.init.material_characteristic(max([GEOM.L, GEOM.B]), diss_fct);
% =========================================================================

% DEFINITION OF THE BOUNDARIES............
NODE_BOUNDARIES = phase_field.fem.bc.set_boundaries(...
    'fix_X', NODESETS('left'),...
    'fix_Y', NODESETS('bottom_left'),...
    'disp_X', NODESETS('right'),...
    'non_hom_pf', NODESETS('crack') ...
);
%.........................................

% DOFS DEFINITION.........................
DOFS = phase_field.init.dofs(MESH, NODE_BOUNDARIES);
%.........................................

% =========================================================================

% =========================================================================
% QUADRATURE
% =========================================================================
QUADRATURE = phase_field.init.quadrature(MESH.dim);

stress_state = phase_field.StressState.PlaneStrain;
% =========================================================================

% =========================================================================
%   SOLVER PARAMETERS
% =========================================================================
SOL_PAR = phase_field.fem.solver.params();

SOL_STAG_PAR = phase_field.fem.solver.stag.params('max_iter', 2 * SOL_PAR.max_iter_pf);

SOL_STEP_PAR = phase_field.fem.solver.step.params(...
    'n_step', 10, ...
    'ux_final', 1/30 ...
);
