% Copyright (C) 2021-2023 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% ========STEP ITERATION===================================================
for i_step = 1:SOL_STEP_PAR.n_step
    disp('************************************************')
    disp([' STEP Nr.: ', num2str(i_step)]);
    disp('************************************************')

    [displ, res_displ, D_RHS_tract] = phase_field.fem.solver.step.pre_iter_update(...
        i_step, sys, displ, SOL_STEP_PAR...
    );

    res_displ_0 = norm(res_displ(sys.DOFS.active_dof,1));

    % ========STAGGERED ITERATION==============================================
    for i_stag = 1:SOL_STAG_PAR.max_iter
        disp([' STAG ITER Nr.: ', num2str(i_stag), '---------------------------']);

        % ======NEWTON-RAPHSON ITERATION FOR THE EQUILIBRIUM EQUATION==========
        [res_0, res, displ, strain_en_undgr, ~, non_conv] = phase_field.fem.solver.newton_raphson(...
            assembly_equilibrium_fh,...
            sys,...
            displ, 0.0, p_field,...
            0.0,...
            sys.DOFS.active_dof,...
            sys.STIFFNESS_MATRIX.i_row,...
            sys.STIFFNESS_MATRIX.j_col,...
            'D_RHS_tract', D_RHS_tract,...
            'max_iter', SOL_PAR.max_iter_displ,...
            'res_tol', SOL_PAR.tol_displ,...
            'line_search', SOL_STEP_PAR.line_search...
        );

        % =======NEWTON-RAPHSON ITERATION FOR THE PHASE FIELD EQUATION=========
        [i_res_pf_0, res_pf, p_field, ~, history_vars_new, non_conv] = phase_field.fem.solver.newton_raphson(...
            assembly_pf_fh,...
            sys,...
            p_field, p_field_old,...
            strain_en_undgr,...
            history_vars_old,...
            sys.DOFS.active_dof_pf,...
            sys.STIFFNESS_MATRIX.i_row_pf,...
            sys.STIFFNESS_MATRIX.j_col_pf,...
            'max_iter', SOL_PAR.max_iter_pf,...
            'res_tol', SOL_PAR.tol_p_field,...
            'line_search', SOL_STEP_PAR.line_search...
        );
        if i_stag == 1
            res_pf_0 = i_res_pf_0;
        end

        % ==================CHECK FOR STAGGERED CONVERGENCE====================
        [SOL_STAG_VAR, strain_en_undgr, stag_converged, sys.STIFFNESS_MATRIX.KK] = phase_field.fem.solver.stag.post_iter_update(...
            assembly_equilibrium_fh, i_stag, SOL_STAG_VAR,...
            sys,...
            D_RHS_tract,...
            p_field, displ,...
            res_pf_0, res_pf, res_displ_0,...
            SOL_STAG_PAR...
        );
        if stag_converged
            % update the history variables
            p_field_old = p_field;
            [~, ~, ~, history_vars_old] = assembly_pf_fh(...
                sys.MESH, sys.DOFS, sys.GEOM.t,...
                sys.QUADRATURE, sys.MAT_CHAR, sys.CC,...
                p_field, p_field_old, strain_en_undgr,...
                sys.stress_state.as_number,...
                history_vars_old...
            );
            break;
        end
    end
    if i_stag == SOL_STAG_PAR.max_iter
        warning('staggered loop did not converge')
    end
    
    % ============================POST-PROCESSING=============================
    SOL_STEP_VAR = phase_field.fem.solver.step.post_iter_update(...
        i_step, SOL_STEP_VAR,...
        sys.STIFFNESS_MATRIX, displ, sys.NODE_BOUNDARIES, sys.MESH.num_node,...
        SOL_STEP_PAR, example_name...
    );

    i_SOL_STAG_VAR = SOL_STAG_VAR(1:i_stag, :);

    indexCols = [i_step * ones(1,i_stag) ; 1:i_stag]';
    indexColumnHeader = {'Load Step', 'iter'};
    cols = [res_displ_0 * ones(i_stag,1), res_pf_0 * ones(i_stag,1),...
        i_SOL_STAG_VAR.el_en, i_SOL_STAG_VAR.fract_en, i_SOL_STAG_VAR.tot_en,...
        i_SOL_STAG_VAR.normal_res_displ, i_SOL_STAG_VAR.normal_res_pf,...
        i_SOL_STAG_VAR.normal_var_en_tot, i_SOL_STAG_VAR.normal_res,...
        i_SOL_STAG_VAR.normal_var_res...
    ];
    columnHeader = {'RES_displ(0)', 'RES_PF(0)', 'Elast_en', 'Fract_en', 'Total_en', 'RES_displ', 'RES_PF', 'Norm_EN_tot', 'Norm_RES_tot', 'Norm_var_RES'};
    logPath = [example_name filesep 'CONV_' example_name sprintf('%05d', i_step) '.log'];

    phase_field.mex.output.dat_output(indexCols, indexColumnHeader, cols, columnHeader, logPath);

    % ============================CRACK PROCESSING=============================
    
    %crack tip tracking (only for the pulltest)
    if ismember(CRACK_PAR.specimen, {'pulltest'})
        %find nodes to ignore (x<0.2L are not considered to not catch boundary effects)
        irrel_DOFs = find(sys.MESH.node(:,1)<=0.2);
        p_field_rel = p_field;
        p_field_rel(irrel_DOFs) = 0;
        [CRACK_VAR] = phase_field.fem.crack.crack_regularized(i_step, sys, CRACK_PAR, CRACK_VAR, p_field_rel, example_name);
        [CRACK_VAR] = phase_field.fem.crack.crack_discretized(i_step, sys, CRACK_PAR, CRACK_VAR, p_field_rel, example_name);
        [CRACK_VAR] = phase_field.fem.crack.crack_interpolated(i_step, sys, CRACK_PAR, CRACK_VAR, p_field_rel, example_name);

        % stop the computations once crack has reached at least 0.75L
        if CRACK_VAR.crack_discretized(end,3) > 0.75 * sys.GEOM.L
            break;
        end
    end

    % ============================PARAVIEW OUTPUT=============================
    if mod(i_step,SOL_STEP_PAR.output_freq) == 0
        [history_vars_L2, stress_L2] = phase_field.mex.fem.processing.L2_PROJECTION(...
            sys.MESH, sys.DOFS, sys.GEOM.t, sys.num_materials, sys.QUADRATURE, sys.CC, p_field,...
            1, history_vars_old...
        );
    
        file_name = [example_name filesep example_name sprintf('%05d', i_step) '.vtk'];
        stress_L2_matrix = sparse(sys.STIFFNESS_MATRIX.i_row_eps, sys.STIFFNESS_MATRIX.j_col_eps, stress_L2);
        phase_field.output.paraview_step(...
            file_name, sys.STIFFNESS_MATRIX, sys.MESH,...
            {'Phase-field', 'Displacement'},...
            {p_field, reshape(displ, [sys.MESH.num_node, 2])},...
            {'Strain', 'Stress', 'History-var'},...
            {...
             reshape(sys.STIFFNESS_MATRIX.eps * displ, [sys.MESH.num_node, 3]),...
             reshape(stress_L2_matrix * displ, [sys.MESH.num_node, 3]),...
             history_vars_L2(:, 1)...
            }...
        );
    end

    SOL_STAG_VAR = phase_field.fem.solver.stag.vars(SOL_STAG_PAR);

    res_displ_0 = 0;
    res_pf_0    = 0;
end
