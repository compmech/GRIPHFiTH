% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
% =========================================================================

split_type = 'MIEHE';

diss_fct = 'AT2';

irrev   = 'PENALTY';

example_name = 'SENS';

% =========================================================================
%   MATERIAL & MODEL PROPERTIES
% =========================================================================
[MESH, GEOM, NODESET, ELEMSET] = specimen.internal.sens('notch_type', 'sharp_notch');

MAT_CHAR = phase_field.init.material_characteristic(GEOM.L, diss_fct, 'E', 210000, 'ni', 0.3, 'Gc', 2.7, 'ell', 0.01);

NODE_BOUNDARIES = phase_field.fem.bc.set_boundaries(...
    'fix_X', NODESET.bot_edge,...
    'fix_Y', [NODESET.bot_edge; NODESET.top_edge],...
    'disp_X', NODESET.top_edge,...
    'non_hom_pf', NODESET.crack ...
);

DOFS = phase_field.init.dofs(MESH, NODE_BOUNDARIES);

QUADRATURE = phase_field.init.quadrature(MESH.dim);

stress_state = phase_field.StressState.PlaneStrain;


% =========================================================================
%   SOLVER PARAMETERS
% =========================================================================
SOL_PAR = phase_field.fem.solver.params();

SOL_STAG_PAR = phase_field.fem.solver.stag.params('max_iter', 2 * SOL_PAR.max_iter_pf);

SOL_STEP_PAR = phase_field.fem.solver.step.params(...
    'n_step', 100, ...
    'ux_final', 0.015 ...
);
