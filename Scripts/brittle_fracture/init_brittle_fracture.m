% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

% =========================================================================

%create empty output directory for input file
if exist(example_name,'dir')
    rmdir(example_name, 's');
end
mkdir(example_name);

displ       = zeros(DOFS.num_dof, 1);
p_field     = zeros(MESH.num_node, 1);
p_field(DOFS.non_hom_dirichlet_bc_pf, 1) = 1;
p_field_old = zeros(size(p_field));
res_p_field = zeros(size(p_field));

history_vars_old = zeros(MESH.num_elem, QUADRATURE.num_gauss_pts, 1);

SIG0 = sparse(MESH.tensors * MESH.num_node, DOFS.num_dof);

SOL_STAG_VAR = phase_field.fem.solver.stag.vars(SOL_STAG_PAR);
SOL_STEP_VAR = phase_field.fem.solver.step.vars(SOL_STEP_PAR);

if exist('CRACK_PAR', 'var')
    CRACK_VAR = phase_field.fem.crack.vars(CRACK_PAR);
else
    CRACK_PAR = phase_field.fem.crack.params(MAT_CHAR, diss_fct,'specimen', 'other');
    CRACK_VAR = phase_field.fem.crack.vars(CRACK_PAR);
end

% =========================================================================
sys = phase_field.System(...
    MAT_CHAR, GEOM, QUADRATURE, DOFS, NODE_BOUNDARIES, SOL_STEP_PAR,...
    MESH, stress_state, p_field, diss_fct...
);

% Prepare assembly function handles
assembly_pf_fh = str2func(...
    sprintf('phase_field.mex.fem.assembly.pf.%s_%s', diss_fct, irrev)...
);
assembly_equilibrium_fh = str2func(...
    sprintf('phase_field.mex.fem.assembly.equilibrium.%s', split_type)...
);

%clear duplicates from memory
clear MAT_CHAR GEOM QUADRATURE DOFS NODE_BOUNDARIES MESH stress_state split_type diss_fct
