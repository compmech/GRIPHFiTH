% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
example_name = 'TEST_shear';

% SPLIT..........................
% ISO     -> Isotropic model
% AMOR    -> Amor vol./dev. split
% MIEHE   -> Spectral split based on the sign of the trace
% FREDDI  -> Freddi bruco split

split_type = 'ISO';
%................................

% DISSIPATION FUNCTION...........
% AT1     -> Linear dissipation w(d) = d
% AT2     -> Quadratic dissipation w(d) = d^2

diss_fct = 'AT2';
%................................

% IRREVERSIBILITY CONDITION......
% HISTORY  -> With history variable [Miehe2010]
% PENALTY  -> With penalty parameter [Gerasimov_DeLorenzis2018]

irrev   = 'HISTORY';
%................................
% =========================================================================

% =========================================================================
%   MESH AND PROBLEM SETUP
% =========================================================================
% MESH AND GEOMETRY.......................
[MESH, GEOM, NODESETS, ELEMSETS] = specimen.internal.plate();
%................................

% =========================================================================
%   MATERIAL & MODEL PROPERTIES
% =========================================================================
MAT_CHAR = phase_field.init.material_characteristic(...
    max([GEOM.L, GEOM.B]),...
    diss_fct,...
    'ell', 0.004...
);

% DEFINITION OF THE BOUNDARIES............
NODE_BOUNDARIES = phase_field.fem.bc.set_boundaries(...
    'fix_X', NODESETS('bottom'),...
    'fix_Y', NODESETS('bottom'),...
    'disp_X', NODESETS('top') ...
);
%.........................................

% DOFS DEFINITION.........................
DOFS = phase_field.init.dofs(MESH, NODE_BOUNDARIES);
%.........................................

% =========================================================================

% =========================================================================
%   QUADRATURE
% =========================================================================
QUADRATURE = phase_field.init.quadrature(MESH.dim);

stress_state = phase_field.StressState.PlaneStrain;
% =========================================================================

% =========================================================================
%   SOLVER PARAMETERS
% =========================================================================
SOL_PAR = phase_field.fem.solver.params();

SOL_STEP_PAR = phase_field.fem.solver.step.params('ux_final', 1.0);
