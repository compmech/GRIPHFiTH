function [testCase] = runPfTest(testCase, inputScriptName)
%RUNPFTEST Wrapper for running inputScriptName and the 'compute_pf' scripts.
%   ...
%

% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

    testCase.TestData.inputScriptName = inputScriptName;

    eval(inputScriptName);
    testCase.TestData.example_name = example_name;

    if strcmp(testCase.TestData.testFolderName,'brittle_fracture')
        init_brittle_fracture;
        solve_brittle_fracture;
    elseif strcmp(testCase.TestData.testFolderName,'elasticity')
        init_elasticity;
        solve_elasticity;
    end
    testCase.TestData.n_step = SOL_STEP_PAR.n_step;

    % verify existence of output files
    testCase.TestData.loadDisplFilename = sprintf('load_displ_%s%s', ...
        testCase.TestData.example_name, '.out');
    verifyEqual(testCase, ...
        exist(fullfile(testCase.TestData.example_name, testCase.TestData.loadDisplFilename), 'file'), 2, ...
        sprintf('Load_displ_output file "%s" not found.', ...
            testCase.TestData.loadDisplFilename));
    testCase.TestData.vtkFilenames = {};
    for i = testCase.TestData.n_step:-1:1
        vtkFilename = sprintf('%s%05d%s', testCase.TestData.example_name, i, '.vtk');
        testCase.TestData.vtkFilenames{i} = vtkFilename;
        verifyEqual(testCase, ...
            exist(fullfile(testCase.TestData.example_name, testCase.TestData.vtkFilenames{i}), 'file'), 2, ...
            sprintf('VTK file "%s" not found.', vtkFilename));
    end
    testCase.TestData.logFilenames = {};
    if isfield(testCase.TestData, 'ifLogFilenames') && testCase.TestData.ifLogFilenames
        for i = testCase.TestData.n_step:-1:1
            logFilename = sprintf( ...
                'CONV_%s%05d%s', testCase.TestData.example_name, i, '.log');
            testCase.TestData.logFilenames{i} = logFilename;
            verifyEqual(testCase, ...
                exist(fullfile(testCase.TestData.example_name, testCase.TestData.logFilenames{i}), 'file'), 2, ...
                sprintf('Log file "%s" not found.', logFilename));
        end
    end


    % verify content of output files
    pathComputedOutputExample = testCase.TestData.example_name;
    pathExpectedOutputExample = test_helpers.pathExpectedOutput(testCase);
    loadDisplContentComputed = test_helpers.readLoadDispl( ...
        fullfile(pathComputedOutputExample, testCase.TestData.loadDisplFilename));
    loadDisplContentExpected = test_helpers.readLoadDispl( ...
        fullfile(pathExpectedOutputExample, testCase.TestData.loadDisplFilename));
    verifyEqual(testCase, loadDisplContentComputed, loadDisplContentExpected, ...
        'AbsTol', 1e-3, 'RelTol', 5e-2, ...
        sprintf('Content check failed for: %s', testCase.TestData.loadDisplFilename));
    for vtkFilenameCell = testCase.TestData.vtkFilenames
        vtkFilename = vtkFilenameCell{1};
        vtkContentComputed = read_vtk(fullfile(pathComputedOutputExample, vtkFilename));
        vtkContentExpected = read_vtk( ...
            fullfile(pathExpectedOutputExample, vtkFilename));
        verifyEqual(testCase, vtkContentComputed, vtkContentExpected, ...
            'AbsTol', 1e-12, 'RelTol', 1e-2, ...
            sprintf('Content check failed for: %s', vtkFilename));
    end
    if isfield(testCase.TestData, 'ifLogContent') && testCase.TestData.ifLogContent
        for logFilenameCell = testCase.TestData.logFilenames
            logFilename = logFilenameCell{1};
            convTable = readtable(fullfile(pathComputedOutputExample, logFilename));
            convTableExpected = readtable(fullfile(pathExpectedOutputExample, logFilename));
            % % allow convergence tables to differ by max. 1 or up to 11% steps count
            % nrow = size(convTable, 1);
            % nrowExpected = size(convTableExpected, 1);
            % nrowDiffFail = max(2, ceil(0.11 * max(nrow, nrowExpected)));
            % verifyTrue(testCase, ...
            %     abs(nrow - nrowExpected) < nrowDiffFail, ...
            %     sprintf('Convergence steps number does not match in: %s', logFilename));
            % nrow = min(nrow, nrowExpected);
            verifyEqual(testCase, ...
                convTable(end, 5:9), convTableExpected(end, 5:9), ...
                'AbsTol', 7.5e-2, 'RelTol', 5e-2, ...
                sprintf('Convergence table does not match in: %s', logFilename));
        end
    end

end
