function [testCase] = setupOnceFolders(testCase)
%SETUPONCEFOLDERS ...
%   ...
%

% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

    % change to scripts folder
    import matlab.unittest.fixtures.CurrentFolderFixture
    testCase.TestData.testFolder = pwd;
    testCase.TestData.runFolder = fullfile( ...
        '..', 'Scripts', testCase.TestData.testFolderName);
    testCase.applyFixture(CurrentFolderFixture(testCase.TestData.runFolder));
    disp(['Scripts folder: '  pwd]);
    import matlab.unittest.fixtures.PathFixture
    f = testCase.applyFixture(PathFixture(fullfile('..', '..', 'Sources')));
    disp(['Added to path: ' f.Folder])
    f = testCase.applyFixture(PathFixture(fullfile('..', '..', 'Dependencies'), ...
        'IncludingSubfolders', true));
    disp(['Added to path: ' f.Folder ', and subfolders'])
end

