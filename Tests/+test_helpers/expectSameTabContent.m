function expectSameTabContent(testCase, pathA, pathB)
%EXPECTSAMETABCONTENT .
%   ...
%

% Copyright (C) 2023 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

    contents = {fileread(pathA), fileread(pathB)};
    for i=1:2
        contents{i} = regexprep(contents{i}, '\r', '');
        contents{i} = replace(contents{i}, 'E+000', '');
        contents{i} = regexprep(contents{i}, '\.([1-9]*)0*', '.$1');
        contents{i} = regexprep(contents{i}, ' ( +)', '\t');
        contents{i} = regexprep(contents{i}, '[\t ]\n', '\n');
    end
    verifyEqual(testCase, contents{1}, contents{2});
end
