function [tests] = elasticityTest()
%ELASTICITYTEST Regression tests for all elasticity examples
%   ...
%

% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

    tests = functiontests({@setupOnce, @teardownOnce, @setup, @teardown, ...
        @testOutputPfTensile, ...
        @testOutputPfShear ...
    });

end

%% Once setup and teardown
function setupOnce(testCase)
    testCase.TestData.testFolderName = 'elasticity';

    testCase = test_helpers.setupOnceFolders(testCase);

    format shortE
end


function teardownOnce(testCase)
    format
end


%% Per test setup and teardown
function setup(testCase)
end


function teardown(testCase)
    test_helpers.deletePfOutput(testCase.TestData);
end


%% Tests
function [testCase] = testOutputPfTensile(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_TENSILE');

end


function [testCase] = testOutputPfShear(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_SHEAR');

end
