function [tests] = brittleFractureTest()
%BRITTLEFRACTURETEST Regression tests for all brittle fracture examples
%   ...
%

% Copyright (C) 2021 ETH Zurich, SIS ID and CompMech D-MAVT
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%    http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.

    tests = functiontests({@setupOnce, @teardownOnce, @setup, @teardown, ...
        @testOutputPf1, ...
        @testOutputPf2, ...
        @testOutputPf3, ...
        @testOutputPf4, ...
        @testOutputPf5, ...
        @testOutputPf6, ...
        @testOutputPf7, ...
        @testOutputPf8, ...
        @testOutputPf9, ...
        @testOutputPf10, ...
        @testOutputPf11, ...
        @testOutputPf12, ...
        @testOutputPf13, ...
        @testOutputPf14, ...
        @testOutputPf15, ...
        @testOutputPf16 ...
    });

end

%% Once setup and teardown
function setupOnce(testCase)
    testCase.TestData.testFolderName = 'brittle_fracture';
    testCase.TestData.ifLogFilenames = true;
    testCase.TestData.ifLogContent = true;

    testCase = test_helpers.setupOnceFolders(testCase);

    format shortE
end


function teardownOnce(testCase)
    format
end


%% Per test setup and teardown
function setup(testCase)
end


function teardown(testCase)
    test_helpers.deletePfOutput(testCase.TestData);
end


%% Tests
function [testCase] = testOutputPf1(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_1');

end


function [testCase] = testOutputPf2(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_2');

end


function [testCase] = testOutputPf3(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_3');

end


function [testCase] = testOutputPf4(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_4');

end


function [testCase] = testOutputPf5(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_5');

end


function [testCase] = testOutputPf6(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_6');

end


function [testCase] = testOutputPf7(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_7');

end


function [testCase] = testOutputPf8(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_8');

end


function [testCase] = testOutputPf9(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_9');

end


function [testCase] = testOutputPf10(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_10');

end


function [testCase] = testOutputPf11(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_11');

end


function [testCase] = testOutputPf12(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_12');

end


function [testCase] = testOutputPf13(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_13');

end


function [testCase] = testOutputPf14(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_14');

end


function [testCase] = testOutputPf15(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_15');

end


function [testCase] = testOutputPf16(testCase)

    testCase = test_helpers.runPfTest(testCase, 'INPUT_16');

end
