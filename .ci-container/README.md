# CI Container README

The Docker image is based on https://github.com/mathworks-ref-arch/matlab-dockerfile
(license file included).

## Create or update CI image

1. Get a MATLAB license file.

   1.  Go the [IT Shop](https://itshop.ethz.ch/) and order floating MATLAB license for
       Linux.
   2.  After order is confirmed, got to the respective MATLAB product description and

       download the `license.dat` file.

2. Build the CI image for the corresponding MATLAB version; e.g. for R2021b:

    ```
    $ MATLAB_RELEASE=r2021b && docker build \
        -t registry.ethz.ch/compmech/griphfith:matlab-${MATLAB_RELEASE} \
        --build-arg MATLAB_RELEASE=${MATLAB_RELEASE} \
        --build-arg MLM_LICENSE_FILE=/path/to/file/MATLAB_R2021b/license.dat .
    ```

    If you encounter the following error during MEX setup step:

    ```
    Unable to access preferences folder /home/matlab/.matlab/R2021b. Preferences folder must be writable.
    ```

    simply re-run the build.

3. Push the CI image to the registry:

    ```
    docker login registry.ethz.ch/compmech
    docker push registry.ethz.ch/compmech/griphfith:matlab-r2021b
    ```
